import Vue from "vue";

import editor from "vue-block-editor";

Vue.component("app-editor", editor);
